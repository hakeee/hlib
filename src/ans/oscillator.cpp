//
// Created by hakeee on 5/13/15.
//

#include <math.h>
#include "oscillator.h"

ans::oscillator::oscillator(unsigned samplerate, double freq, double phase):
        sound() {
    this->sampleRate = samplerate;
    this->frequenze = freq;
    this->startPhase = phase;
    this->increse = freq / (samplerate);
    this->channels = 1;
}

double ans::oscillator::get(accessor* id, int channel) {
    (void)channel;
    oscaccessor* op = static_cast<oscaccessor*>(id);
    double val = sin(op->phase);
    op->phase += increse;
    if(op->phase >= 2 * M_PI)
        op->phase -= 2 * M_PI;

    return val;
}

ans::sound::accessor* ans::oscillator::playOn(ans::stream &stm) {
    oscaccessor* tmp = new oscaccessor;
    tmp->phase = startPhase;
    tmp->sound = this;
    ans::sound::playOn(stm, static_cast<accessor*>(tmp));
    return tmp;
}
