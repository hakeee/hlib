//
// Created by hakeee on 5/13/15.
//

#include "sound.h"
#include "stream.h"

ans::sound::sound(const std::string &file, bool preload, int channels, unsigned int sampleRate, int format) {

    if(channels < 1 || channels > 2) {
        fprintf(stderr, "Only mono or stereo supported!");
        return;
    };
    this->volume = new float[channels];
    std::fill_n(this->volume, channels, 1.0f);
    this->loaded = preload;
    this->format = format;
    this->channels = channels;
    this->sampleRate = sampleRate;

    handle = SndfileHandle(file, SFM_READ, format, channels, sampleRate);

    if(handle.error() != SF_ERR_NO_ERROR) {
        fprintf(stderr, "Couldn't read \"%s\": %s", file.c_str(), handle.strError());
        return;
    }

    sf_count_t dataSize = handle.frames() * handle.channels();
    frames.resize(static_cast<unsigned long>(dataSize), 0);
    if(preload) {
        handle.read(&frames[0], dataSize);
        readed = dataSize;
    } else
        readed = 0;
}

void ans::sound::playOn(ans::stream &stm, accessor* acc) {
    stm.accessors.push_back(acc);
}

double ans::sound::get(ans::sound::accessor* id, int channel) {
    double val = 0;
    if(loaded) {
        val = frames[id->where + (channels != ANS_MONO ? channel : 0)];
    } else {
        if(id->where > readed) {
            handle.read(&frames[id->where], handle.channels());
            if(handle.error() != SF_ERR_NO_ERROR) {
                fprintf(stderr, "Error couldn't read: %s", handle.strError());
                return 0;
            }
            readed += handle.channels();
        }
        val = frames[id->where + (channels != ANS_MONO ? channel : 0)];
    }
    //id.where++;
    if(id->where >= frames.size()) {
        id->where = 0;
        if(!id->looping) {
            id->remove = true;
        }
        //call finished callback
    }
    return val * volume[channel];
}
