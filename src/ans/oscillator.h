//
// Created by hakeee on 5/13/15.
//

#ifndef ANS_EXEC_OSCILLATOR_H
#define ANS_EXEC_OSCILLATOR_H

#include "sound.h"

namespace ans {
    class oscillator:
            public sound {

        struct oscaccessor:
                public accessor {
            double phase;   /* Each channel gets its' own frequency. */
        };
        double startPhase;
        double frequenze;
        double increse;
    public:
        oscillator(unsigned samplerate, double freq, double phase = 0.0);

        double get(accessor* id) {
            return get(id, 1);
        };

        double get(accessor* id, int channel) override;

        accessor* playOn(stream &stm);

        bool sync(double phase) = delete;
    };
};

#endif //ANS_EXEC_OSCILLATOR_H
