//
// Created by hakeee on 5/16/15.
//

#include <stdio.h>
#include "stream.h"

ans::stream::stream():
        stm(nullptr), pdi(nullptr) {

}

bool ans::stream::init(int deviceIndex, int channels, int samplerate, int latency, unsigned format, unsigned frames) {
    DEBUG_ASSERT(stm == nullptr);

    framebuffersize = frames;
    sampleRate = samplerate;
    volume = 1.0f;

    pdi = Pa_GetDeviceInfo(deviceIndex);
    outputParameters.device = deviceIndex;
    outputParameters.channelCount = channels;
    if(outputParameters.channelCount > pdi->maxOutputChannels)
        outputParameters.channelCount = pdi->maxOutputChannels;
    outputParameters.sampleFormat = format;              /* 32 bit floating point output */
    if(latency == ANS_LAT_LOW)
        outputParameters.suggestedLatency = pdi->defaultLowOutputLatency;
    else
        outputParameters.suggestedLatency = pdi->defaultHighOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;
    outputParameters.sampleFormat |= paNonInterleaved;
    err = Pa_OpenStream(
            &stm,
            NULL,               /* No input. */
            &outputParameters,
            samplerate,         /* Sample rate. */
            frames,             /* Frames per BufferVI. */
            paClipOff,          /* Samples never out of range, no clipping. */
            patestCallback,
            this
    );
    //PaAlsa_EnableRealtimeScheduling(&stm, 1);
    if(err != paNoError)
    ansErr(err);

    //Pa_SetMaxThreadPriority(&stm);
    return true;
}

bool ans::stream::start() {
    err = Pa_StartStream(stm);
    if(err != paNoError) {
        ansErr(err);
        return false;
    }
    return true;
}

bool ans::stream::stop() {
    err = Pa_StopStream(stm);
    if(err != paNoError) {
        ansErr(err);
        return false;
    }
    return true;
}

int ans::stream::patestCallback(
        const void* inputBuffer, void* outputBuffer, unsigned long framesPerBuffer,
        const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags,
        void* userData
) {
    ans::stream* stm = static_cast<ans::stream*>(userData);
    float** outputs = (float**)outputBuffer;
    (void)inputBuffer;
    (void)timeInfo;
    (void)statusFlags;
    double max = 0;
    for(int frameIndex = 0; frameIndex < (int)framesPerBuffer; frameIndex++) {
        for(int channelIndex = 0; channelIndex < stm->outputParameters.channelCount; channelIndex++) {
            /* Output sine wave on every channel. */
            float val = 0;
            for(auto &&arg : stm->accessors) {
                val += arg->sound->get(arg, channelIndex);
            }

            outputs[channelIndex][frameIndex] = val;
            if(max < (val > 0 ? val : -val))
                max = (val > 0 ? val : -val);

            outputs[channelIndex][frameIndex] *= stm->volume;
        }
        stm->accessors.remove_if(
                [&stm](ans::sound::accessor*&arg)->bool {
                    arg->where += arg->sound->getChannels();//stm->outputParameters.channelCount;
                    if(arg->remove) {
                        delete arg;
                        return true;
                    }
                    return false;
                }
        );
    }
    if(max > 1)
        for(int frameIndex = 0; frameIndex < (int)framesPerBuffer; frameIndex++)
            for(int channelIndex = 0; channelIndex < stm->outputParameters.channelCount; channelIndex++)
                outputs[channelIndex][frameIndex] /= max;


    return 0;
}
