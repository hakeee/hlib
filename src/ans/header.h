//
// Created by hakeee on 5/19/15.
//

#ifndef ANS_EXEC_HEADER_H
#define ANS_EXEC_HEADER_H

namespace ans {
    enum {
        ANS_MONO = 1,
        ANS_STEREO = 2,
    };
    enum {
        ANS_LAT_HIGH,
        ANS_LAT_LOW,
    };
};


#ifdef ___DEBUG
// 0xCC - int 3 - breakpoint
// 0x90 - nop?
//#define __emit__(code) (asm __volatile__(code))
#define DebugInt3 asm __volatile__(".byte 0x90CC")
#define DEBUG_ASSERT(expr) if (!(expr)) DebugInt3;
#else
#define DebugInt3
#define DEBUG_ASSERT(expr) //assert(expr)
#endif

#define ansErr(err) \
printf("Error: %s - %s - %d.\n", __FILE__, __PRETTY_FUNCTION__, __LINE__);\
fprintf(stderr, "An error occured while using the portaudio stream\n");\
fprintf(stderr, "Error number: %d\n", err );\
fprintf(stderr, "Error message: %s\n", Pa_GetErrorText(err));\


#endif //ANS_EXEC_HEADER_H
