//
// Created by hakeee on 5/16/15.
//

#ifndef ANS_EXEC_CONNECTION_H
#define ANS_EXEC_CONNECTION_H


#include <portaudio.h>
#include "header.h"
#include "sound.h"
#include <list>

namespace ans {
    class stream {
        PaStream* stm;
        PaStreamParameters outputParameters;
        PaError err;
        const PaDeviceInfo* pdi;


        std::list<ans::sound::accessor*> accessors;


        float volume;

        int sampleRate;
        unsigned framebuffersize;

        static int patestCallback(
                const void* inputBuffer,
                void* outputBuffer,
                unsigned long framesPerBuffer,
                const PaStreamCallbackTimeInfo* timeInfo,
                PaStreamCallbackFlags statusFlags,
                void* userData
        );

    public:

        stream();

        bool init(
                int deviceIndex, int channels = ANS_MONO, int samplerate = 44100, int latency = ANS_LAT_LOW,
                unsigned format = paFloat32, unsigned frames = 256
        );

        bool start();

        bool stop();

        inline float getVolume() const {
            return volume;
        }

        inline void setVolume(float volume) {
            stream::volume = volume >= 0 ? (volume <= 1.0f ? volume : 1.0f) : 0.0f;
        }


        friend void ans::sound::playOn(ans::stream &stm, accessor*);
    };
}

#endif //ANS_EXEC_CONNECTION_H
