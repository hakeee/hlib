//
// Created by hakeee on 5/13/15.
//

#ifndef ANS_EXEC_SOUND_H
#define ANS_EXEC_SOUND_H

#include <string>
#include <vector>
#include <sndfile.hh>
#include "header.h"

namespace ans {
    class stream;

    class sound {
    public:
        struct accessor {
            ans::sound* sound;
            unsigned where;
            bool remove = false;
            bool looping;
        };
    protected:
        int channels;
        /* Mono / Stereo */
        unsigned sampleRate;
        /* What */

        bool loaded;
        /* Block fileload or load as needed */
        int format;
        SndfileHandle handle;
        std::vector<float> frames;
        long readed;
        float* volume;

        void playOn(ans::stream &stm, accessor* acc);

        sound() {
        };
    public:
        sound(
                const std::string &file,
                bool preload = true,
                int channels = 1,
                unsigned int sampleRate = 44100,
                int format = SF_FORMAT_WAV | SF_FORMAT_PCM_16
        );

        virtual ~sound() {
            delete[](volume);
        };

        virtual double get(accessor* id, int channel);

        virtual accessor* playOn(stream &stm, bool looping) {
            accessor* tmp = new accessor;
            tmp->sound = this;
            tmp->where = 0;
            tmp->remove = false;
            tmp->looping = looping;
            playOn(stm, tmp);
            return tmp;
        };

        inline float getVolume(int channel) const {
            return volume[channel];
        }

        inline void setVolume(int channel, float volume) {
            sound::volume[channel] = volume >= 0 ? (volume <= 1.0f ? volume : 1.0f) : 0.0f;
        }

        inline int getChannels() {
            return channels;
        }

        friend ans::stream;
    };
}

#endif //ANS_EXEC_SOUND_H
