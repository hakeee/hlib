//
// Created by hakeee on 5/27/15.
//

#ifndef ANS_EXEC_SOUND3D_H
#define ANS_EXEC_SOUND3D_H

#include <tgmath.h>
#include "sound.h"

namespace ans {

    class sound3d:
            public sound {
    public:
        union position3d {
            float v[3];
            struct {
                float x;
                float y;
                float z;
            };

            position3d():
                    x(0), y(0), z(0) {
            }

            position3d(float x, float y, float z):
                    x(x), y(y), z(z) {
            }
        };

        struct accessor3d:
                public accessor {
            position3d pos;
        };

        static position3d listnerpos[2];

        sound3d(
                const std::string &file,
                bool preload = true,
                int channels = 1,
                unsigned int sampleRate = 44100,
                int format = SF_FORMAT_WAV | SF_FORMAT_PCM_16
        ):
                sound(file, preload, channels, sampleRate, format) {
        };

        accessor3d* playOn(stream &stm, bool looping, position3d pos) {
            accessor3d* tmp = new accessor3d;
            tmp->sound = this;
            tmp->where = 0;
            tmp->remove = false;
            tmp->looping = looping;
            tmp->pos = pos;
            sound::playOn(stm, tmp);
            return tmp;
        };
    private:
        accessor* playOn(stream &stm, bool looping) {
            (void)stm;
            (void)looping; static_assert(true, "This function should not be used for playing sounds!"); return nullptr;
        };
    public:
        virtual double get(accessor* id, int channel) override {
            double val = sound::get(id, channel);

            position3d pos = static_cast<accessor3d*>(id)->pos;
            position3d listner;
            if(channel >= 0 && channel < 2) {
                listner = listnerpos[channel];

                double distance = cbrt(
                        (pos.x - listner.x) * (pos.x - listner.x) +
                        (pos.y - listner.y) * (pos.y - listner.y) +
                        (pos.z - listner.z) * (pos.z - listner.z));
                if(distance > 7)
                    return 0;

                val *= 1 - distance / 7.0;
            }
            return val;
        };

        friend ans::stream;
    };
}

#endif //ANS_EXEC_SOUND3D_H
