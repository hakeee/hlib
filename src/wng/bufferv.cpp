//
// Created by hakeee on 9/11/15.
//

#include "bufferv.h"

hlib::wng::BufferV::BufferV() {
    glGenBuffers(1, &vertexId_);
}

hlib::wng::BufferV::~BufferV() {
    glDeleteBuffers(1, &vertexId_);
}

int hlib::wng::BufferV::GetVertexCount() {
    return vertexCount_;
}

void hlib::wng::BufferV::Use() {
    glBindBuffer(GL_ARRAY_BUFFER, vertexId_);
}

void hlib::wng::BufferV::SetVertexData(unsigned int count, float* array) {
    vertexCount_ = count;
    glBindBuffer(GL_ARRAY_BUFFER, vertexId_);
    glBufferData(GL_ARRAY_BUFFER, count * sizeof(float), array, GL_STATIC_DRAW);
}