//
// Created by hakeee on 9/11/15.
//

#include "buffervi.h"

hlib::wng::BufferVI::BufferVI() {
    glGenBuffers(1, &vertexId_);
    glGenBuffers(1, &indexId_);
}

hlib::wng::BufferVI::~BufferVI() {
    glDeleteBuffers(1, &vertexId_);
    glDeleteBuffers(1, &indexId_);
}

int hlib::wng::BufferVI::GetVertexCount() {
    return vertexCount_;
}

int hlib::wng::BufferVI::GetIndicesCount() {
    return indexCount_;
}

void hlib::wng::BufferVI::Use() {
    glBindBuffer(GL_ARRAY_BUFFER, vertexId_);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexId_);
}

void hlib::wng::BufferVI::SetVertexData(unsigned int count, float* array) {
    vertexCount_ = count;
    glBindBuffer(GL_ARRAY_BUFFER, vertexId_);
    glBufferData(GL_ARRAY_BUFFER, count * sizeof(float), array, GL_STATIC_DRAW);
}

void hlib::wng::BufferVI::SetIndices(unsigned int count, unsigned int* array) {
    indexCount_ = count;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexId_);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int), array, GL_STATIC_DRAW);
}