//
// Created by hakeee on 7/5/15.
//

#include <tuple>
#include <fstream>
#include "shader.h"
#include "../utl/logg.h"

using namespace hlib::wng;

bool shader::Load(std::string &&filename) {
    GLuint vs, is;

    std::ifstream in, in2;
    in.open(filename + ".vert", std::ios_base::in);

    if(!in.is_open()) {
        hlib::logg::print("Couldn't open file at \"", filename + ".vert", "\"");
        return false;
    }

    std::string shaderSource;
    shaderSource.assign(std::istreambuf_iterator<char>(in), std::istreambuf_iterator<char>());
    in.close();

    vs = glCreateShader(GL_VERTEX_SHADER);
    if(vs == 0) {
        hlib::logg::print("glCreateShader failed!");
        return false;
    }

    const GLchar* shaderSourceChar = shaderSource.c_str();
    glShaderSource(vs, 1, &shaderSourceChar, 0);
    glCompileShader(vs);

    GLint shaderCompiled = GL_FALSE;
    glGetShaderiv(vs, GL_COMPILE_STATUS, &shaderCompiled);
    if(shaderCompiled != GL_TRUE) {
        GLint maxLength = 0;
        glGetShaderiv(vs, GL_INFO_LOG_LENGTH, &maxLength);

        GLint logLength = 0;
        GLchar* log = new char[maxLength];
        glGetShaderInfoLog(vs, maxLength, &logLength, log);
        if(logLength > 0)
            printf("%s\n", log);

        delete[] log;
        glDeleteShader(vs);
        return false;
    }
    //glGetError();
    in2.open(filename + ".frag", std::ios_base::in);

    if(!in2.is_open()) {
        hlib::logg::print("Couldn't open file at \"", filename + ".frag", "\"");
        return false;
    }

    shaderSource = std::string();
    shaderSource.assign(std::istreambuf_iterator<char>(in2), std::istreambuf_iterator<char>());
    in2.close();

    is = glCreateShader(GL_FRAGMENT_SHADER);
    if(is == 0) {
        hlib::logg::print("GL Error: %i\n", glGetError());
        hlib::logg::print("glCreateShader failed!");
        return false;
    }

    shaderSourceChar = shaderSource.c_str();
    glShaderSource(is, 1, &shaderSourceChar, 0);
    //printf("GL Error: %i\n",glGetError());
    glCompileShader(is);

    //printf("GL Error: %i\n",glGetError());
    shaderCompiled = GL_FALSE;
    glGetShaderiv(is, GL_COMPILE_STATUS, &shaderCompiled);

    //printf("GL Error: %i\n",glGetError());
    if(shaderCompiled != GL_TRUE) {
        printf("GL Error: %i\n", glGetError());
        GLint maxLength = 0;
        glGetShaderiv(is, GL_INFO_LOG_LENGTH, &maxLength);

        GLint logLength = 0;
        GLchar* log = new char[maxLength];
        glGetShaderInfoLog(is, maxLength, &logLength, log);

        if(logLength >= 0)
            hlib::logg::print(log, hlib::logg::endl);

        delete[] log;
        glDeleteShader(vs);
        glDeleteShader(is);
        return false;
    }

    program_ = glCreateProgram();
    if(program_ == 0) {
        hlib::logg::print("Couldn't create shader program!");
        glDeleteShader(vs);
        glDeleteShader(is);
        return false;
    }

    glAttachShader(program_, vs);
    glAttachShader(program_, is);
    glLinkProgram(program_);
    glUseProgram(program_);

    GLint linkSuccess = GL_TRUE;
    glGetProgramiv(program_, GL_LINK_STATUS, &linkSuccess);
    if(linkSuccess != GL_TRUE) {
        GLint maxLength = 0;
        glGetProgramiv(program_, GL_INFO_LOG_LENGTH, &maxLength);

        GLint logLength = 0;
        GLchar* log = new char[maxLength];
        glGetShaderInfoLog(program_, maxLength, &logLength, log);
        if(logLength > 0)
            hlib::logg::print(log);
        else
            hlib::logg::print("An error occured while linking shader program but no log was available!");

        glDeleteShader(vs);
        glDeleteShader(is);
        delete[] log;

        return false;
    }

    GLint numActiveAttribs = 0;
    GLint numActiveUniforms = 0;
    glGetProgramiv(program_, GL_ACTIVE_ATTRIBUTES, &numActiveAttribs);
    glGetProgramiv(program_, GL_ACTIVE_UNIFORMS, &numActiveUniforms);

    GLint maxAttribNameLength = 0;
    glGetProgramiv(program_, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &maxAttribNameLength);
    std::vector<GLchar> nameDataAttrib((unsigned long)maxAttribNameLength);
    for(int attrib = 0; attrib < numActiveAttribs; ++attrib) {
        GLint arraySize = 0;
        GLenum type = 0;
        GLsizei actualLength = 0;
        glGetActiveAttrib(
                program_, (GLuint)attrib, (GLsizei)nameDataAttrib.size(), &actualLength, &arraySize, &type,
                &nameDataAttrib[0]
        );
        std::string name(&nameDataAttrib[0], (unsigned long)actualLength);
        if(glGetAttribLocation(program_, name.c_str()) != 9) {
            attribs_.push_back(std::make_tuple(name, type, arraySize));
            stride += arraySize * GetFloatCount(type) * sizeof(float);
        }
    }

    printf("stride: %lu\n", stride / sizeof(float));
    glGenVertexArrays(1, &varr_);
    glBindVertexArray(varr_);

    size_t offset = 0;
    for(unsigned int i = 0; i < attribs_.size(); i++) {
        GLuint p = (GLuint)glGetAttribLocation(program_, std::get<0>(attribs_[i]).c_str());
        glEnableVertexAttribArray(p);
        glVertexAttribPointer(
                p, std::get<2>(attribs_[i]) * GetFloatCount(std::get<1>(attribs_[i])), GL_FLOAT, GL_FALSE,
                stride, (void*)offset
        );
        printf("Name: %s, Offset: %lu, P:%i\n", std::get<0>(attribs_[i]).c_str(), offset / sizeof(float), p);
        offset += std::get<2>(attribs_[i]) * GetFloatCount(std::get<1>(attribs_[i])) * sizeof(float);
    }

    glBindVertexArray(0);

    std::vector<GLchar> nameDataUni(256);
    for(int unif = 0; unif < numActiveUniforms; ++unif) {
        GLint arraySize = 0;
        GLenum type = 0;
        GLsizei actualLength = 0;
        glGetActiveUniform(
                program_, (GLuint)unif, (GLsizei)nameDataUni.size(), &actualLength, &arraySize, &type, &nameDataUni[0]
        );
        std::string name(&nameDataUni[0], (unsigned long)actualLength);
        uniforms_[name] = std::make_tuple(type, arraySize, nullptr);
    }
    return true;
}

void shader::Draw(BufferVI object, bool triangel_strip) {
    glUseProgram(program_);

    object.Use();

    int texturesCount = 0;
    for(auto uniform: uniforms_) {
        int p = glGetUniformLocation(program_, uniform.first.c_str());
        if(std::get<2>(uniform.second) == nullptr) {
            printf("Uniform with name \"%s\" has not been set!\n", uniform.first.c_str());
            continue;
        }
        GLfloat* value = static_cast<GLfloat*>(std::get<2>(uniform.second));
        switch(std::get<0>(uniform.second)) {
            case GL_FLOAT_MAT4:
                glUniformMatrix4fv(p, std::get<1>(uniform.second), GL_FALSE, value);
                break;
            case GL_FLOAT:
                glUniform1fv(p, std::get<1>(uniform.second), value);
                break;
            case GL_SAMPLER_2D:
                glActiveTexture((GLenum)(GL_TEXTURE0 + texturesCount));
                glBindTexture(GL_TEXTURE_2D, *reinterpret_cast<GLuint*>(std::get<2>(uniform.second)));
                glUniform1i(p, texturesCount);
                texturesCount++;
                break;
            default:
                printf(
                        "Uniform shadername:\"%s\"Enum code:%i, hex: %x, not implemented in \"int GLShader::Uniform(GLenum lenum)\"\n",
                        uniform.first.c_str(),
                        std::get<0>(uniform.second),
                        std::get<0>(uniform.second));
        }
    }

    glBindVertexArray(varr_);

    glDrawElements(
            triangel_strip ? GL_TRIANGLE_STRIP : GL_TRIANGLES,      // mode
            object.GetIndicesCount(),    // count
            GL_UNSIGNED_INT,   // type
            (void*)0           // element array BufferVI offset
    );

    glBindVertexArray(0);

    for(; texturesCount > 0; texturesCount--) {
        glActiveTexture((GLenum)(GL_TEXTURE0 + texturesCount - 1));
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glUseProgram(0);
}

int shader::GetFloatCount(GLenum lenum) {
    switch(lenum) {
        case GL_FLOAT_VEC3:
            return 3;
        case GL_FLOAT_VEC2:
            return 2;
        case GL_FLOAT:
            return 1;
        default:
            printf("Enum code:%i, not implemented in \"int GLShader::GetFloatCount(GLenum lenum)\"", lenum);
    }
    return 0;
}

shader::~shader() {
    glDeleteProgram(program_);
}

void shader::SetUniform(const std::string &name, void* value) {
    auto uniform_it = uniforms_.find(name);
    if(uniform_it != uniforms_.end())
        std::get<2>(uniform_it->second) = value;
}

void shader::SetUniform(const std::string &name, int value) {
    auto uniform_it = uniforms_.find(name);
    if(uniform_it != uniforms_.end())
        std::get<2>(uniform_it->second) = reinterpret_cast<void*>(value);
}

void shader::Draw(BufferV object, bool triangel_strip) {
    glUseProgram(program_);

    object.Use();

    int texturesCount = 0;
    for(auto uniform: uniforms_) {
        int p = glGetUniformLocation(program_, uniform.first.c_str());
        if(std::get<2>(uniform.second) == nullptr) {
            printf("Uniform with name \"%s\" has not been set!\n", uniform.first.c_str());
            continue;
        }
        GLfloat* value = static_cast<GLfloat*>(std::get<2>(uniform.second));
        switch(std::get<0>(uniform.second)) {
            case GL_FLOAT_MAT4:
                glUniformMatrix4fv(p, std::get<1>(uniform.second), GL_FALSE, value);
                break;
            case GL_FLOAT:
                glUniform1fv(p, std::get<1>(uniform.second), value);
                break;
            case GL_SAMPLER_2D:
                glActiveTexture((GLenum)(GL_TEXTURE0 + texturesCount));
                glBindTexture(GL_TEXTURE_2D, *reinterpret_cast<GLuint*>(std::get<2>(uniform.second)));
                glUniform1i(p, texturesCount);
                texturesCount++;
                break;
            default:
                printf(
                        "Uniform shadername:\"%s\"Enum code:%i, hex: %x, not implemented in \"int GLShader::Uniform(GLenum lenum)\"\n",
                        uniform.first.c_str(),
                        std::get<0>(uniform.second),
                        std::get<0>(uniform.second));
        }
    }

    glBindVertexArray(varr_);
    glDrawArrays(
            triangel_strip ? GL_TRIANGLE_STRIP : GL_TRIANGLES,      // mode
            0,                 // first
            (GLsizei)(object.GetVertexCount() / (stride / sizeof(float))) // count
    );
    glBindVertexArray(0);

    for(; texturesCount > 0; texturesCount--) {
        glActiveTexture((GLenum)(GL_TEXTURE0 + texturesCount - 1));
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glUseProgram(0);
}
