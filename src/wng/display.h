//
// Created by hakeee on 5/4/15.
//

#ifndef WNG_EXEC_DISPLAY_H
#define WNG_EXEC_DISPLAY_H

#include <X11/Xlib.h>
#include <string>

namespace hlib {
    namespace wng {
        class display {
            /* this variable will be used to store the "default" screen of the  */
            /* X server. usually an X server has only one screen, so we're only */
            /* interested in that screen.                                       */
            int screen_num;

            /* this variable will be used to store the ID of the root window of our */
            /* screen. Each screen always has a root window that covers the whole   */
            /* screen, and always exists.                                           */
            Window root_window;

            std::string name;

            Display* d;

        public:
            display(const char* dname);

            ~display();

            friend class window;
        };
    }
}

#endif //WNG_EXEC_DISPLAY_H
