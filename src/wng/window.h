//
// Created by hakeee on 5/4/15.
//

#ifndef WNG_WINDOW_H
#define WNG_WINDOW_H

#include <GL/glew.h>
#include <GL/glx.h>
#include <GL/glu.h>
#include "display.h"
#include <functional>
#include <mutex>
#include <condition_variable>
#include <thread>

namespace hlib {
    namespace wng {
        int UseMultithreadning();

        class window {
            display d;
            int x, y;

            GLint att[5];
            XVisualInfo* vi;
            Colormap cmap;
            XSetWindowAttributes swa;
            Window win;
            GLXContext glc;
            XWindowAttributes gwa;
            XEvent xev;

            std::function<void(XEvent)> ckey;
            std::function<void(XEvent)> cexp;
            std::function<void(XEvent)> cmus;

            std::thread mainth;

            static void runf(wng::window* win);

            static int errorHandler(Display* disp, XErrorEvent* error);

        public:
            window(const char* Dname/*, bool multithreading*/);

            ~window();

            bool Init(
                    std::string Title, unsigned int width, unsigned int height, int posx, int posy,
                    unsigned int boarder_width
            );
            //void Map();

            void SetKeyCallback(std::function<void(XEvent)> callback);

            void SetExposeCallback(std::function<void(XEvent)> callback);

            void SetMouseCallback(std::function<void(XEvent)> callback);

            void Run(bool block);

            void WaitForExit();

            unsigned int CharToKeycode(const char* c) const;

            void Redraw();

            void Flush();

            void SendEvent(XEvent event);

            void CloseEvent();

            void PrintGLVersion();

            void SetCursorPosition(int x, int y);

            int GetWidth();

            int GetHeight();
        };

    }
}
#endif //WNG_WINDOW_H
