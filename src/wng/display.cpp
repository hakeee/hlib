//
// Created by hakeee on 5/4/15.
//

#include <stdlib.h>
#include <stdio.h>
#include "display.h"

hlib::wng::display::display(const char* dname):
        name(dname) {
    d = XOpenDisplay(dname);
    if(d == nullptr) {
        fprintf(stderr, "Cannot connect to X server %s\n", dname);
        exit(-1);
    }

    /* check the number of the default screen for our X server. */
    screen_num = DefaultScreen(d);

    /* find the width of the default screen of our X server, in pixels. */
    //screen_width = DisplayWidth(d, screen_num);

    /* find the height of the default screen of our X server, in pixels. */
    //screen_height = DisplayHeight(d, screen_num);

    /* find the ID of the root window of the screen. */
    root_window = RootWindow(d, screen_num);

    /* find the value of a white pixel on this screen. */
    //white_pixel = WhitePixel(d, screen_num);

    /* find the value of a black pixel on this screen. */
    //black_pixel = BlackPixel(d, screen_num);
}

hlib::wng::display::~display() {
    XCloseDisplay(d);
}
