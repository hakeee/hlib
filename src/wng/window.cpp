//
// Created by hakeee on 5/4/15.
//

#include <stdio.h>
#include <GL/glew.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <string.h>
#include <iostream>
#include "window.h"

hlib::wng::window::window(const char* Dname/*, bool multithreading*/):
        d(Dname) {
/*    if(multithreading)
        hlib::wng::UseMultithreadning();*/
    XSetErrorHandler(hlib::wng::window::errorHandler);
}

hlib::wng::window::~window() {
}

bool hlib::wng::window::Init(
        std::string Title,
        unsigned int width,
        unsigned int height,
        int posx, int posy,
        unsigned int boarder_width
) {

    this->x = posx;
    this->y = posy;
    int tatt[] = {GLX_RGBA,
                  GLX_DEPTH_SIZE, 24,
                  GLX_DOUBLEBUFFER,
                  None};
    memcpy(this->att, tatt, sizeof(int) * 5);

    //w = XCreateSimpleWindow(d.d, d.root_window, x, y, width, height, boarder_width, boarder_color, backround_color);
    vi = glXChooseVisual(d.d, 0, tatt);

    if(vi == nullptr) {
        fprintf(stderr, "No appropiete visual found");
        return false;
    }

    cmap = XCreateColormap(d.d, d.root_window, vi->visual, AllocNone);
    swa.colormap = cmap;
    swa.event_mask = ExposureMask | KeyPressMask | KeyReleaseMask |
                     ButtonPress | ButtonRelease | PointerMotionMask;

    win = XCreateWindow(
            d.d, d.root_window, x, y, width, height, boarder_width,
            vi->depth, InputOutput, vi->visual, CWColormap | CWEventMask | CWBorderPixel, &swa
    );

    //XMapWindow(d.d, win);
    XStoreName(d.d, win, Title.c_str());

    // Can shange to be used over network with GL_FALSE instead.

    glc = glXCreateContext(d.d, vi, NULL, GL_TRUE);
    glXMakeCurrent(d.d, win, glc);

    GLenum glew = glewInit();
    if(glew != GLEW_OK)
        printf("Glew Error:%i", glew);

    //glEnable(GL_DEPTH);
    return true;
}

void hlib::wng::window::SetKeyCallback(std::function<void(XEvent)> callback) {
    ckey = callback;
}

// std::function<void(XEvent)>
void hlib::wng::window::SetExposeCallback(std::function<void(XEvent)> callback) {
    cexp = callback;
}

void hlib::wng::window::SetMouseCallback(std::function<void(XEvent)> callback) {
    cmus = callback;
}

void hlib::wng::window::Run(bool block) {
    //std::lock_guard<std::mutex> lk(exitm);
    if(block) {
        runf(this);
    } else {
        mainth = std::thread(runf, this);
    }

}

void hlib::wng::window::WaitForExit() {
    //::unique_lock<std::mutex> lk(exitm);
    //exitcond.wait(lk);
    mainth.join();
}

int hlib::wng::window::errorHandler(Display* disp, XErrorEvent* error) {
    char errorText[512];
    //char errorData[512];
    XGetErrorText(disp, error->error_code, errorText, 512);
    //XGetErrorDatabaseText(disp, "wng_exec", "idk","A error occourd", errorData, 512);
    fprintf(stderr, "ErrorText: %s\n", errorText);
    return 0;
}

void hlib::wng::window::runf(hlib::wng::window* win) {
    XMapWindow(win->d.d, win->win);
    glXMakeCurrent(win->d.d, win->win, win->glc);
    Atom wmDeleteMessage = XInternAtom(win->d.d, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(win->d.d, win->win, &wmDeleteMessage, 1);
    bool running = true;
    while(running) {
        XNextEvent(win->d.d, &win->xev);
        //std::cout << win->xev.type << std::endl;
        if(win->xev.xclient.data.l[0] == static_cast<long>(wmDeleteMessage)) {
            //XUnmapWindow(win->d.d, win->win);
            glXDestroyContext(win->d.d, win->glc);
            XDestroyWindow(win->d.d, win->win);
            XFlush(win->d.d);
            running = false;
        } else if(win->xev.type == Expose) {
            XGetWindowAttributes(win->d.d, win->win, &win->gwa);
            glViewport(0, 0, win->gwa.width, win->gwa.height);
            if(win->cexp) win->cexp(win->xev);
            glXSwapBuffers(win->d.d, win->win);
        } else if(win->xev.type == KeyPress || win->xev.type == KeyRelease) {
            if(win->ckey) win->ckey(win->xev);
        } else if(win->xev.type == ButtonPress || win->xev.type == ButtonRelease || win->xev.type == MotionNotify) {
            if(win->cmus) win->cmus(win->xev);
        } else if(win->xev.type == DestroyNotify)
            running = false;
        //win->exitcond.notify_all();
    }
    return;
}

unsigned int hlib::wng::window::CharToKeycode(const char* c) const {
    return XKeysymToKeycode(d.d, XStringToKeysym(c));
}

void hlib::wng::window::Redraw() {
    //XExposeEvent ev;// en event struct som kan skickas med XSendEvent(..)
    //ev.type = Expose;
    XEvent event;
    event.type = Expose;
    //event.xexpose = ev;
    SendEvent(event);
}

void hlib::wng::window::Flush() {
    XFlush(d.d);
    XSync(d.d, 0);
}

void hlib::wng::window::SendEvent(XEvent event) {
    XSendEvent(d.d, win, 1, 0, &event);
}

void hlib::wng::window::CloseEvent() {
    Atom wmDeleteMessage = XInternAtom(d.d, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(d.d, win, &wmDeleteMessage, 1);

    XClientMessageEvent clm;
    clm.type = ClientMessage;
    clm.message_type = wmDeleteMessage;
    clm.data.l[0] = wmDeleteMessage;
    clm.display = d.d;
    clm.format = 32;
    clm.send_event = false;

    XEvent event;
    event.type = ClientMessage;
    event.xclient = clm;
    SendEvent(event);
}

int ::hlib::wng::UseMultithreadning() {
    return XInitThreads();
}

void hlib::wng::window::PrintGLVersion() {
    /* we can now get data for the specific OpenGL instance we created */
    const GLubyte* renderer = glGetString(GL_RENDERER);
    const GLubyte* vendor = glGetString(GL_VENDOR);
    const GLubyte* version = glGetString(GL_VERSION);
    const GLubyte* glslVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
    GLint major, minor;
    glGetIntegerv(GL_MAJOR_VERSION, &major);
    glGetIntegerv(GL_MINOR_VERSION, &minor);
    printf("GL Vendor : %s\n", vendor);
    printf("GL Renderer : %s\n", renderer);
    printf("GL Version (string) : %s\n", version);
    printf("GL Version (integer) : %d.%d\n", major, minor);
    printf("GLSL Version : %s\n", glslVersion);
}

void hlib::wng::window::SetCursorPosition(int x, int y) {
    XWarpPointer(d.d, None, win, 0, 0, 0, 0, x, y);
    XFlush(d.d);
}

int hlib::wng::window::GetWidth() {
    return gwa.width;
}

int hlib::wng::window::GetHeight() {
    return gwa.height;
}
