//
// Created by hakeee on 9/11/15.
//

#ifndef WNG_BUFFERVI_H
#define WNG_BUFFERVI_H

#include <GL/glew.h>

namespace hlib {
    namespace wng {
        class BufferVI {
            GLuint vertexId_, indexId_;
            int vertexCount_, indexCount_;

        public:
            BufferVI();

            ~BufferVI();

            int GetVertexCount();

            int GetIndicesCount();

            void Use();

            void SetVertexData(unsigned int count, float* array);

            void SetIndices(unsigned int count, unsigned int* array);
        };
    };
};

#endif //WNG_BUFFERVI_H
