//
// Created by hakeee on 9/11/15.
//

#ifndef WNG_BUFFERV_H
#define WNG_BUFFERV_H

#include <GL/glew.h>

namespace hlib {
    namespace wng {
        class BufferV {
            GLuint vertexId_;
            int vertexCount_;

        public:
            BufferV();

            ~BufferV();

            int GetVertexCount();

            void Use();

            void SetVertexData(unsigned int count, float* array);
        };
    };
};

#endif //WNG_BUFFERV_H
