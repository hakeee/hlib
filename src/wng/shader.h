//
// Created by hakeee on 7/5/15.
//

#ifndef WNG_SHADER_H
#define WNG_SHADER_H

#include <GL/glew.h>
#include <vector>
#include <string>
#include <map>
#include "buffervi.h"
#include "bufferv.h"

namespace hlib {
    namespace wng {
        class shader {
            GLuint program_, varr_, stride = 0;
            //                          namn    type    count
            std::vector<std::tuple<std::string, GLenum, GLint>> attribs_;
            //        namn                   type    count  pointer
            std::map<std::string, std::tuple<GLenum, GLint, void*>> uniforms_;

            int GetFloatCount(GLenum);

        public:
            ~shader();

            bool Load(std::string &&filename);

            void SetUniform(const std::string &name, void* value);

            void SetUniform(const std::string &name, int value);

            void Draw(BufferVI object, bool triangel_strip = false);

            void Draw(BufferV object, bool triangel_strip = false);
        };
    };
};

#endif //WNG_SHADER_H
