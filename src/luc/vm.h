#ifndef LUAVM_H__
#define LUAVM_H__

#include "detail/vm_d.h"
#include <map>
#include <iostream>

namespace hlib {
    namespace luc {

        class LuaVM {

        public:
            LuaVM():
                    lua_state(nullptr) {
            }

            ~LuaVM() {
                lua_close(lua_state);
            }

            bool Load() {
                if(lua_state != nullptr) lua_close(lua_state);
                lua_state = luaL_newstate();  /* create state */
                if(lua_state == NULL) {
                    std::cerr << "cannot create state: not enough memory";
                    return EXIT_FAILURE;
                }
                luaL_openlibs(lua_state);  /* open standard libraries */

                RegisterGlobal("LVM", this);

                return EXIT_SUCCESS;
            }

            ////////////////////////////////////////////////////////////////////
            ///
            ///	Push
            ///
            ////////////////////////////////////////////////////////////////////
            void Push() {
            };

            template<typename Arg, typename... Args>
            inline void Push(Arg &&arg, Args &&... args);

            ////////////////////////////////////////////////////////////////////
            ///
            ///	Pop
            ///
            ////////////////////////////////////////////////////////////////////
            void Pop() {
            };

            template<typename Arg, typename... Args>
            inline std::tuple<Arg, Args...> Pop();

            template<typename Arg>
            Arg PopOne();

            ////////////////////////////////////////////////////////////////////
            ///
            ///	Register
            ///
            ////////////////////////////////////////////////////////////////////
            template<typename T>
            inline void RegisterGlobal(const std::string &name, T &&data);

            ////////////////////////////////////////////////////////////////////
            ///
            ///	Get
            ///
            ////////////////////////////////////////////////////////////////////
            template<typename T>
            T GetGlobal(const std::string &name);

            ////////////////////////////////////////////////////////////////////
            ///
            ///	Call
            ///
            ////////////////////////////////////////////////////////////////////
            inline void Call(const std::string &name) {
                lua_getglobal(lua_state, name.c_str());
                lua_call(lua_state, 0, 0);
            };

            template<typename... Args>
            inline void Call(const std::string &name, Args... args) {
                lua_getglobal(lua_state, name.c_str());
                Push(args...);
                lua_pcall(lua_state, sizeof...(Args), 0, 0);
                return;
            };

            template<typename... Returns>
            inline std::tuple<Returns...> Call(const std::string &name) {
                lua_getglobal(lua_state, name.c_str());
                lua_pcall(lua_state, 0, sizeof...(Returns), 0);
                return Pop<Returns...>();
            };

            //template<typename Return, typename Arg>
            //inline Return Call(const std::string& name, Arg arg) {
            //	lua_getglobal(lua_state, name.c_str());
            //	Push(arg);
            //	lua_pcall(lua_state, 1, 1, 0);
            //	return PopOne<Return>();
            //};

            //template<typename... Returns, typename... Args>
            //inline std::tuple<Returns...> Call(const std::string& name, Args... args) {
            //	lua_getglobal(lua_state, name.c_str());
            //	Push(args...);
            //	lua_pcall(lua_state, sizeof...(Args), sizeof...(Returns), 0);
            //	return Pop<Returns...>();
            //};


            ////////////////////////////////////////////////////////////////////
            ///
            ///	Bind
            ///
            ////////////////////////////////////////////////////////////////////

//    template<typename T>
//    void Bind(const std::string& str, std::function<T> func) {
//        auto con = detail::LuaConnector<T>(func);
//        detail::BaseConnector::Bind(lua_state, str, &con);
//    }

#define ToLua(type, func) _LuaVMDetail::LuaConnector<type>(func)
            std::map<const std::string, detail::BaseConnector*> functions;

            template<typename Ret, typename... Args>
            void Bind(const std::string &str, detail::LuaConnector<Ret(Args...)> &&func) {
                if(functions[str] != nullptr) delete functions[str];
                functions[str] = new detail::LuaConnector<Ret(Args...)>(func);
                detail::BaseConnector::Bind(lua_state, str, functions[str]);
            }

            template<typename Ret, typename... Args>
            void Bind(const std::string &str, std::function<Ret(Args...)> func) {
                if(functions[str] != nullptr) delete functions[str];
                functions[str] = new detail::LuaConnector<Ret(Args...)>(func);
                detail::BaseConnector::Bind(lua_state, str, functions[str]);
            }

            template<typename... Args>
            void Bind(const std::string &str, std::function<void(Args...)> func) {
                if(functions[str] != nullptr) delete functions[str];
                functions[str] = new detail::LuaConnector<void(Args...)>(func);
                detail::BaseConnector::Bind(lua_state, str, functions[str]);
            }

            void Bind(const std::string &str, std::function<void(void)> func) {
                if(functions[str] != nullptr) delete functions[str];
                functions[str] = new detail::LuaConnector<void(void)>(func);
                detail::BaseConnector::Bind(lua_state, str, functions[str]);
            }

            ////////////////////////////////////////////////////////////////////
            ///
            ///	Execute
            ///
            ////////////////////////////////////////////////////////////////////
            void ExecuteString(const std::string &string) {
                if(luaL_dostring(lua_state, string.c_str())) {
                    printf("%s\n", lua_tostring(lua_state, -1));
                }
            }

            void ExecuteFile(const std::string &filename) {
                if(luaL_dofile(lua_state, filename.c_str())) {
                    printf("%s\n", lua_tostring(lua_state, -1));
                }
            }

            lua_State* GetState() const {
                return lua_state;
            }

        private:
            lua_State* lua_state;
        };

        template<class Arg, class ...Args>
        inline void LuaVM::Push(Arg &&arg, Args &&...args) {
            detail::VM_N_Push(lua_state, std::forward<Arg>(arg), std::forward<Args>(args)...);
        }

        template<class Arg, class ...Args>
        inline std::tuple<Arg, Args...> LuaVM::Pop() {
            return detail::VM_Pop<Arg, Args...>(lua_state);
        }

        template<class Arg>
        inline Arg LuaVM::PopOne() {
            return std::get<0>(detail::VM_Pop<Arg>(lua_state));
        }

        template<class T>
        inline void LuaVM::RegisterGlobal(const std::string &name, T &&data) {
            Push<T>(std::forward<T>(data));
            lua_setglobal(lua_state, name.c_str());
        }

        template<class T>
        inline T LuaVM::GetGlobal(const std::string &name) {
            lua_getglobal(lua_state, name.c_str());
            return std::get<0>(detail::VM_Pop<T>(lua_state));
        }
    }
}
#endif //LUAVM_H__
