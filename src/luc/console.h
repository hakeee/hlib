#pragma once

#include "vm.h"
#include <streambuf>

namespace hlib {
    namespace luc {
        class console {
        public:
            console();

            ~console();

            bool Run();

            void SetInSteam(std::istream* stream);

            void SetOutSteam(std::ostream* stream);

            void SetLuaState(lua_State* state);

        private:
            int Addreturn() const;

            int Incomplete(int status) const;

            int Multiline() const;

            int Loadline() const;

            const char* GetPrompt(int firstline) const;

            int Pushline(int firstline) const;

            static int Msghandler(lua_State* L);

            int Docall(int narg, int nres) const;

            int Report(int status) const;

            void Print() const;

            // TODO: remake to use an error stream if one is avalible
            void ErrorMessage(const char* pname, const char* msg) const;

            std::istream* in;
            std::ostream* out;

            lua_State* lst;
        };

    }
}
