#include "console.h"
#include <string.h>

#if !defined(LUA_PROMPT)
#define LUA_PROMPT        "> "
#define LUA_PROMPT2        ">> "
#endif

#if !defined(LUA_PROGNAME)
#define LUA_PROGNAME        "lua"
#endif

#if !defined(LUA_MAXINPUT)
#define LUA_MAXINPUT        512
#endif

#if !defined(LUA_INIT_VAR)
#define LUA_INIT_VAR        "LUA_INIT"
#endif

#define lua_readline(lst, b, p) \
        ((void)lst, fputs(p, out), fflush(out),  /* show prompt */ \
        fgets(b, LUA_MAXINPUT, in) != NULL)  /* get line */
#define lua_saveline(lst, idx)    { (void)lst; (void)idx; }
#define lua_freeline(lst, b)    { (void)lst; (void)b; }
/* mark in error messages for incomplete statements */
#define EOFMARK        "<eof>"
#define marklen        (sizeof(EOFMARK)/sizeof(char) - 1)

using namespace hlib::luc;

console::console():
        in(&std::cin), out(&std::cout) {
    lst = nullptr;
}


console::~console() {
}

bool console::Run() {
    // print lua version
    lua_writestring(LUA_COPYRIGHT, strlen(LUA_COPYRIGHT));
    lua_writeline();
    //
    int status;
    while((status = Loadline()) != -1) {
        if(status == LUA_OK)
            status = Docall(0, LUA_MULTRET);
        if(status == LUA_OK) Print();
        else Report(status);
    }
    lua_settop(lst, 0);  /* clear stack */
    lua_writeline();
    return EXIT_SUCCESS;
}

void console::SetInSteam(std::istream* stream) {
    if(stream) in = stream;
    else in = &std::cin;
    return;
}

void console::SetOutSteam(std::ostream* stream) {
    if(stream) out = stream;
    else out = &std::cout;
    return;
}

void console::SetLuaState(lua_State* state) {
    lst = state;
}

/*
** Try to compile line on the stack as 'return <line>'; on return, stack
** has either compiled chunk or original line (if compilation failed).
*/
int console::Addreturn() const {
    int status;
    size_t len;
    const char* line;
    lua_pushliteral(lst, "return ");
    lua_pushvalue(lst, -2);  /* duplicate line */
    lua_concat(lst, 2);  /* new line is "return ..." */
    line = lua_tolstring(lst, -1, &len);
    if((status = luaL_loadbuffer(lst, line, len, "=stdin")) == LUA_OK)
        lua_remove(lst, -3);  /* remove original line */
    else
        lua_pop(lst, 2);  /* remove result from 'luaL_loadbuffer' and new line */
    return status;
}


/*
** Check whether 'status' signals a syntax error and the error
** message at the top of the stack ends with the above mark for
** incomplete statements.
*/
int console::Incomplete(int status) const {
    if(status == LUA_ERRSYNTAX) {
        size_t lmsg;
        const char* msg = lua_tolstring(lst, -1, &lmsg);
        if(lmsg >= marklen && strcmp(msg + lmsg - marklen, EOFMARK) == 0) {
            lua_pop(lst, 1);
            return 1;
        }
    }
    return 0;  /* else... */
}

/*
** Read multiple lines until a complete Lua statement
*/
int console::Multiline() const {
    for(; ;) {  /* repeat until gets a complete statement */
        size_t len;
        const char* line = lua_tolstring(lst, 1, &len);  /* get what it has */
        int status = luaL_loadbuffer(lst, line, len, "=stdin");  /* try it */
        if(!Incomplete(status) || !Pushline(0))
            return status;  /* cannot or should not try to add continuation line */
        lua_pushliteral(lst, "\n");  /* add newline... */
        lua_insert(lst, -2);  /* ...between the two lines */
        lua_concat(lst, 3);  /* join them */
    }
}

/*
** Read a line and try to load (compile) it first as an expression (by
** adding "return " in front of it) and second as a statement. Return
** the final status of load/call with the resulting function (if any)
** in the top of the stack.
*/
int console::Loadline() const {
    int status;
    lua_settop(lst, 0);
    if(!Pushline(1))
        return -1;  /* no input */
    if((status = Addreturn()) != LUA_OK)  /* 'return ...' did not work? */
        status = Multiline();  /* try as command, maybe with continuation lines */
    lua_saveline(lst, 1);  /* keep history */
    lua_remove(lst, 1);  /* remove line from the stack */
    lua_assert(lua_gettop(lst) == 1);
    return status;
}

/*
** Returns the string to be used as a prompt by the interpreter.
*/
const char* console::GetPrompt(int firstline) const {
    const char* p;
    lua_getglobal(lst, firstline ? "_PROMPT" : "_PROMPT2");
    p = lua_tostring(lst, -1);
    if(p == NULL) p = (firstline ? LUA_PROMPT : LUA_PROMPT2);
    return p;
}

/*
** Prompt the user, read a line, and push it into the Lua stack.
*/
int console::Pushline(int firstline) const {
    char buffer[LUA_MAXINPUT];
    //char *b = BufferVI;
    std::string s = buffer;
    size_t l;
    const char* prmt = GetPrompt(firstline);
    (*out) << prmt;
    if(!std::getline((*in), s))
        return 0;  /* no input (prompt will be popped by caller) */
    if(in != &std::cin)
        (*out) << s << std::endl;
    lua_pop(lst, 1);  /* remove prompt */
    l = s.length();//strlen(b);
    if(l > 0 && s[l - 1] == '\n')  /* line ends with newline? */
        s[l - 1] = '\0';  /* remove it */
    if(firstline && s[0] == '=')  /* for compatibility with 5.2, ... */
        lua_pushfstring(lst, "return %s", s.c_str() + 1);  /* change '=' to 'return' */
    else
        lua_pushstring(lst, s.c_str());
    //lua_freeline(lst, b);
    return 1;
}

/*
** Message handler used to run all chunks
*/
int console::Msghandler(lua_State* L) {
    const char* msg = lua_tostring(L, 1);
    if(msg == NULL) {  /* is error object not a string? */
        if(luaL_callmeta(L, 1, "__tostring") &&  /* does it have a metamethod */
           lua_type(L, -1) == LUA_TSTRING)  /* that produces a string? */
            return 1;  /* that is the message */
        else
            msg = lua_pushfstring(
                    L, "(error object is a %s value)",
                    luaL_typename(L, 1));
    }
    luaL_traceback(L, L, msg, 1);  /* append a standard traceback */
    return 1;  /* return the traceback */
}

/*
** Interface to 'lua_pcall', which sets appropriate message function
** and C-signal handler. Used to run all chunks.
*/
int console::Docall(int narg, int nres) const {
    int status;
    int base = lua_gettop(lst) - narg;  /* function index */
    lua_pushcfunction(lst, Msghandler);  /* push message handler */
    lua_insert(lst, base);  /* put it under function and args */
    status = lua_pcall(lst, narg, nres, base);
    lua_remove(lst, base);  /* remove message handler from the stack */
    return status;
}

/*
** Check whether 'status' is not OK and, if so, prints the error
** message on the top of the stack. It assumes that the error object
** is a string, as it was either generated by Lua or by 'msghandler'.
*/
int console::Report(int status) const {
    if(status != LUA_OK) {
        const char* msg = lua_tostring(lst, -1);
        ErrorMessage(nullptr, msg);
        lua_pop(lst, 1);  /* remove message */
    }
    return status;
}

/*
** Prints (calling the Lua 'print' function) any values on the stack
*/
void console::Print() const {
    int n = lua_gettop(lst);
    if(n > 0) {  /* any result to be printed? */
        luaL_checkstack(lst, LUA_MINSTACK, "too many results to print");
        lua_getglobal(lst, "print");
        lua_insert(lst, 1);
        if(lua_pcall(lst, n, 0, 0) != LUA_OK)
            ErrorMessage(
                    nullptr, lua_pushfstring(
                    lst, "error calling 'print' (%s)",
                    lua_tostring(lst, -1)));
    }
}

/*
** Prints an error message, adding the program name in front of it
** (if present)
*/
void console::ErrorMessage(const char* pname, const char* msg) const {
    if(pname) lua_writestringerror("%s: ", pname);
    lua_writestringerror("%s\n", msg);
}