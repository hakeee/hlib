#pragma once

#include "../../utl/tupleindex.h"
#include <lua.hpp>
#include <string>
#include <functional>
#include <tuple>
#include <set>

namespace hlib {
    namespace luc {
        namespace detail {

            ////////////////////////////////////////////////////////////////////
            ///
            ///	Push
            ///
            ////////////////////////////////////////////////////////////////////

            template<typename Arg>
            inline void VM_Push(lua_State* L, Arg &arg) {
                lua_pushlightuserdata(L, &arg);
            };

            template<typename Arg>
            inline void VM_Push(lua_State* L, Arg* arg) {
                if(arg == nullptr) {
                    lua_pushnil(L);
                }
                else {
                    lua_pushlightuserdata(L, arg);
                    /*if (const std::string* metatable = m.Find(typeid(T))) {
                        luaL_setmetatable(l, metatable->c_str());
                    }*/
                }
            };

            inline void VM_Push(lua_State* L, int arg) {
                lua_pushinteger(L, arg);
            };

            inline void VM_Push(lua_State* L, bool arg) {
                lua_pushboolean(L, arg);
            };

            inline void VM_Push(lua_State* L, const char* arg) {
                lua_pushstring(L, arg);
            };

            inline void VM_Push(lua_State* L, float arg) {
                lua_pushnumber(L, arg);
            };

            inline void VM_Push(lua_State* L, std::string &arg) {
                lua_pushlstring(L, arg.c_str(), arg.size());
            };

            inline void VM_Push(lua_State* L, unsigned int arg) {
                lua_pushinteger(L, static_cast<int>(arg));
            };

            inline void VM_Push(lua_State* L, lua_Integer arg) {
                lua_pushinteger(L, arg);
            };

            inline void VM_Push(lua_State* L, lua_Number arg) {
                lua_pushnumber(L, arg);
            };
            //inline void VM_Push(lua_State* L, lua_CFunction arg) {
            //	lua_pushcfunction(L, arg);
            //};

            inline void VM_N_Push(lua_State* L) {
                (void)L;
            };

            template<class Arg, class ...Args>
            inline void VM_N_Push(lua_State* L, Arg &&arg, Args &&...args) {
                VM_N_Push(L, std::forward<Args>(args)...);
                VM_Push(L, std::forward<Arg>(arg));
            };

            ////////////////////////////////////////////////////////////////////
            ///
            ///	Get
            ///
            ////////////////////////////////////////////////////////////////////

            template<typename Arg>
            inline Arg VM_Get(utl::_id<Arg>, lua_State* L, int index) {
                return *(Arg*)lua_touserdata(L, -index);
            };

            template<typename Arg>
            inline Arg* VM_Get(utl::_id<Arg*>, lua_State* L, int index) {
                return lua_touserdata(L, -index);
                //lua_pushlightuserdata(L, &arg);
            };

            inline int VM_Get(utl::_id<int>, lua_State* L, int index) {
                return lua_tointeger(L, -index);
            };

            inline bool VM_Get(utl::_id<bool>, lua_State* L, int index) {
                return (bool)lua_toboolean(L, -index);
            };

            inline char* VM_Get(utl::_id<char*>, lua_State* L, int index) {
                return (char*)lua_tostring(L, -index);
            };

            inline const char* VM_Get(utl::_id<const char*>, lua_State* L, int index) {
                return lua_tostring(L, -index);
            };

            inline float VM_Get(utl::_id<float>, lua_State* L, int index) {
                return lua_tonumber(L, -index);
            };

            inline std::string VM_Get(utl::_id<std::string>, lua_State* L, int index) {
                return lua_tostring(L, -index);
            };

            inline unsigned int VM_Get(utl::_id<unsigned int>, lua_State* L, int index) {
                return static_cast<unsigned int>(lua_tointeger(L, -index));
            };

            inline lua_Integer VM_Get(utl::_id<lua_Integer>, lua_State* L, int index) {
                return lua_tointeger(L, -index);
            };

            inline lua_Number VM_Get(utl::_id<lua_Number>, lua_State* L, int index) {
                return lua_tonumber(L, -index);
            };
            //inline lua_CFunction VM_Get(utl::_id<lua_CFunction>, lua_State* L, int index) {
            //	return lua_tocfunction(L, -index);
            //};

            struct VM_N_Get {
                template<typename... Args, unsigned int... N>
                inline static std::tuple<Args...> Do(lua_State* L, utl::indexed_tuple<N...>) {
                    //return std::make_tuple<Args...>(VM_Get(utl::_id<Args>{}, L, sizeof...(N)-N)...);
                    return std::make_tuple<Args...>(VM_Get(utl::_id<Args>{}, L, N + 1)...);
                };

                template<typename... Args>
                inline static std::tuple<Args...> prepare(lua_State* L) {
                    //typedef utl::to_ituple<Args...> indices;
                    typedef utl::to_rituple<Args...> indices;
                    return Do<Args...>(L, indices());
                };
            };

            template<typename... Args>
            std::tuple<Args...> VM_Get(lua_State* L) {
                std::tuple<Args...> tup = VM_N_Get::prepare<Args...>(L);
                return tup;
            };


            ////////////////////////////////////////////////////////////////////
            ///
            ///	Pop
            ///
            ////////////////////////////////////////////////////////////////////

            template<typename Arg, typename... Args>
            std::tuple<Arg, Args...> VM_Pop(lua_State* L) {
                std::tuple<Arg, Args...> tup = VM_N_Get::prepare<Arg, Args...>(L);
                lua_pop(L, (int)sizeof...(Args) + 1);
                return tup;
            };

            ////////////////////////////////////////////////////////////////////
            ///
            ///	Functions
            ///
            ////////////////////////////////////////////////////////////////////

            //Register functions to lua
            struct BaseConnector {
                virtual int Execute(lua_State* L) = 0;

                static int Binding(lua_State* L) {
                    BaseConnector* base = (BaseConnector*)lua_touserdata(L, lua_upvalueindex(1));
                    return base->Execute(L);
                };

                static void Bind(lua_State* L, const std::string &name, BaseConnector* base) {
                    lua_pushlightuserdata(L, base);
                    lua_pushcclosure(L, &BaseConnector::Binding, 1);
                    lua_setglobal(L, name.c_str());
                };

                virtual ~BaseConnector() {
                };

            protected:
                //static std::set<BaseConnector*> functions;
            };

            template<typename>
            class LuaConnector { };

            template<>
            class LuaConnector<void()>:
                    public BaseConnector {
                std::function<void()> rfunc;
            public:
                //		LuaConnector(void(*pfunc)(Args...)) {
                //			rfunc = pfunc;
                //			functions.emplace(this);
                //		};

                LuaConnector(std::function<void()> pfunc) {
                    rfunc = pfunc;
                    //functions.emplace(this);
                };

                void Do() {
                    rfunc();
                };

                int Execute(lua_State* L) override {
                    (void)L;
                    Do();
                    return 0;
                };
            };

            template<class... Args>
            class LuaConnector<void(Args...)>:
                    public BaseConnector {
                std::function<void(Args...)> rfunc;
            public:
                //		LuaConnector(void(*pfunc)(Args...)) {
                //			rfunc = pfunc;
                //			functions.emplace(this);
                //		};

                LuaConnector(std::function<void(Args...)> pfunc) {
                    rfunc = pfunc;
                    //functions.emplace(this);
                };

                template<unsigned int... N>
                void Do(std::tuple<Args...> args, utl::indexed_tuple<N...>) {
                    rfunc(std::get<N>(args)...);
                };

                int Execute(lua_State* L) override {
                    typedef utl::to_ituple<Args...> indices;
                    Do(VM_Pop<Args...>(L), indices());
                    return 0;
                };
            };

            template<class Ret, class... Args>
            class LuaConnector<Ret(Args...)>:
                    public BaseConnector {
                std::function<Ret(Args...)> rfunc;
            public:
                //		LuaConnector(Ret(*pfunc)(Args...)) {
                //			rfunc = pfunc;
                //			functions.emplace(this);
                //		};

                explicit LuaConnector(std::function<Ret(Args...)> pfunc) {
                    rfunc = pfunc;
                    //functions.emplace(this);
                };

                template<unsigned int... N>
                Ret Do(std::tuple<Args...> args, utl::indexed_tuple<N...>) {
                    return rfunc(std::get<N>(args)...);
                };

                int Execute(lua_State* L) override {
                    typedef utl::to_ituple<Args...> indices;
                    VM_Push(L, Do(VM_Pop<Args...>(L), indices()));
                    return 1;
                };
            };


            template<class... Rets, class... Args>
            class LuaConnector<std::tuple<Rets...>(Args...)>:
                    public BaseConnector {
                std::function<std::tuple<Rets...>(Args...)> rfunc;
            public:
                //		LuaConnector(std::tuple<Rets...>(*pfunc)(Args...)) {
                //			rfunc = pfunc;
                //			functions.emplace(this);
                //		};

                LuaConnector(std::function<std::tuple<Rets...>(Args...)> pfunc) {
                    rfunc = pfunc;
                    //functions.emplace(this);
                };

                template<unsigned int... N>
                std::tuple<Rets...> Do(std::tuple<Args...> args, utl::indexed_tuple<N...>) {
                    return rfunc(std::get<N>(args)...);
                };

                template<unsigned int... N>
                void Return(lua_State* L, std::tuple<Rets...> rets, utl::indexed_tuple<N...>) {
                    return VM_N_Push(L, std::get<N>(rets)...);
                };

                int Execute(lua_State* L) override {
                    typedef utl::to_ituple<Args...> Aindices;
                    typedef utl::to_rituple<Rets...> Rindices;
                    Return(L, Do(VM_Pop<Args...>(L), Aindices()), Rindices());
                    constexpr int rets = sizeof...(Rets);
                    return rets;
                };
            };

            //Connect to lua
            template<typename>
            class Function {
            };

            template<typename... Args>
            class Function<void(Args...)> {
                lua_State* l;
                const char* n;
            public:
                Function(lua_State* L, const char* name):
                        l(L), n(name) {
                };

                void operator()(Args... args) {
                    lua_getglobal(l, n);
                    VM_N_Push(l, std::forward<Args>(args)...);
                    constexpr int argc = sizeof...(Args);
                    lua_pcall(l, argc, 0, 0);
                    return;
                };
            };

            template<typename Ret, typename... Args>
            class Function<Ret(Args...)> {
                lua_State* l;
                const char* n;
            public:
                Function(lua_State* L, const char* name):
                        l(L), n(name) {
                };

                Ret operator()(Args... args) {
                    lua_getglobal(l, n);
                    VM_N_Push(l, std::forward<Args>(args)...);
                    constexpr int argc = sizeof...(Args);
                    lua_pcall(l, argc, 1, 0);
                    return std::get<0>(VM_Pop<Ret>(l));
                };
            };

            template<typename... Rets, typename... Args>
            class Function<std::tuple<Rets...>(Args...)> {
                lua_State* l;
                const char* n;
            public:
                Function(lua_State* L, const char* name):
                        l(L), n(name) {
                };

                std::tuple<Rets...> operator()(Args... args) {
                    lua_getglobal(l, n);
                    VM_N_Push(l, std::forward<Args>(args)...);
                    constexpr int argc = sizeof...(Args);
                    constexpr int retc = sizeof...(Rets);
                    lua_pcall(l, argc, retc, 0);
                    auto r = VM_Pop<Rets...>(l);
                    return r;
                };
            };
        };
    };
};