//
// Created by hakeee on 6/17/15.
//

#ifndef NET_LOGG_H
#define NET_LOGG_H

#include <mutex>
#include <iostream>

namespace hlib {
    namespace logg {
        namespace detail {
            static std::mutex glock;

            inline void pprint() {
            };

            template<typename Arg, typename... Args>
            inline void pprint(Arg &&arg, Args &&... args) {
                std::cout << arg;
                pprint(args...);
            }

        }

        template<typename... Args>
        inline void print(Args &&... args) {
            detail::glock.lock();
            detail::pprint(args...);
            detail::glock.unlock();
        }

        static const char endl = '\n';
    }
}

#endif //NET_LOGG_H
