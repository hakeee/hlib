#pragma once

namespace hlib {
    namespace utl {

        ////////////////////////////////////////////////////////////////////
        ///
        ///	Argument id
        ///
        ////////////////////////////////////////////////////////////////////

        template<class T>
        struct _id {
        };

        ////////////////////////////////////////////////////////////////////
        ///
        ///	Tuple Index
        ///
        ////////////////////////////////////////////////////////////////////

        // A complete index array
        template<unsigned... Indices>
        struct indexed_tuple {
            template<unsigned N>
            using append = indexed_tuple<Indices..., N>;

            template<unsigned N>
            using reverse = indexed_tuple<N, Indices...>;

            //using type = typename indexed_tuple<Indices...>;
        };

        // Creates the indexed array for a tuple
        template<unsigned Size>
        struct make_indexed_tuple {
            typedef typename make_indexed_tuple<Size - 1>::type::template append<Size - 1> type;
        };

        // Creates the indexed array for a tuple
        template<unsigned N>
        struct reverse_indexed_tuple {
            typedef typename reverse_indexed_tuple<N - 1>::type::template reverse<N - 1> type;
        };

        // The last tuple type for this metafunction
        template<>
        struct make_indexed_tuple<0u> {
            typedef indexed_tuple<> type;
        };

        template<>
        struct reverse_indexed_tuple<0u> {
            typedef indexed_tuple<> type;
        };

        // Like a macro to make it easier to use
        template<typename... Args>
        using to_ituple = typename make_indexed_tuple<sizeof...(Args)>::type;

        // Like a macro to make it easier to use
        template<typename... Args>
        using to_rituple = typename reverse_indexed_tuple<sizeof...(Args)>::type;
    }
}