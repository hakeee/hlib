//
// Created by hakeee on 6/17/15.
//

#include "timer.h"
#include <boost/thread.hpp>
#include "../utl/logg.h"

using namespace hlib::net;

void timer::handler(const boost::system::error_code &code, timer &timer) {
    if(code) {
        logg::print("[", boost::this_thread::get_id(), "] Error: ", code, logg::endl);
    } else {
        timer.callback(timer.userdata);
        if(timer.repeting) {
            timer.set_duration(timer.duration);
            timer.sptr_timer->async_wait(
                    timer.sptr_strand->wrap(
                            boost::bind(&timer::handler, _1, timer)));
        }
    }
}

timer::timer(boost::shared_ptr<boost::asio::io_service> io, boost::shared_ptr<boost::asio::strand> strand)
        :
        repeting(false), duration(1), userdata(nullptr) {
    sptr_timer = boost::shared_ptr<boost::asio::deadline_timer>(new boost::asio::deadline_timer(*io));
    sptr_strand = strand;
}

void timer::set_repeting(bool rep) {
    repeting = rep;
}

void timer::set_duration(int64_t dur) {
    sptr_timer->expires_from_now(boost::posix_time::milliseconds(dur));
    duration = dur;
}

void timer::set_userdata(void* data) {
    userdata = data;
}

void timer::set_callback(std::function<void(void*)> function) {
    callback = function;
}

void timer::start() {
    set_duration(duration);
    sptr_timer->async_wait(
            sptr_strand->wrap(
                    boost::bind(&timer::handler, _1, *this)
            )
    );
}
