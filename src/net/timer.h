//
// Created by hakeee on 6/17/15.
//

#ifndef NET_TIMER_H
#define NET_TIMER_H


#include <boost/asio/deadline_timer.hpp>
#include <boost/asio.hpp>
#include <boost/function.hpp>

namespace hlib {
    namespace net {

        class timer {
            boost::shared_ptr<boost::asio::deadline_timer> sptr_timer;
            boost::shared_ptr<boost::asio::strand> sptr_strand;
            bool repeting = false;
            int64_t duration;
            void* userdata;
            std::function<void(void*)> callback;

            static void handler(const boost::system::error_code &code, timer &timer);

        public:
            timer(boost::shared_ptr<boost::asio::io_service> io, boost::shared_ptr<boost::asio::strand> strand);

            void set_repeting(bool rep);

            void set_duration(int64_t dur);

            void set_userdata(void* data);

            void set_callback(std::function<void(void*)> function);

            void start();
        };
    };
};

#endif //NET_TIMER_H
