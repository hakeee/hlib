//
// Created by hakeee on 7/3/15.
//

#define THREADS 4

#include "game.h"
#include "../utl/logg.h"

using namespace hlib::xnb;


void workerfunc(boost::shared_ptr<boost::asio::io_service> io_service) {
    hlib::logg::print("[", boost::this_thread::get_id(), "] Thread Start", hlib::logg::endl);

    while(true) {
        try {
            boost::system::error_code ec;
            io_service->run(ec);
            if(ec) {
                hlib::logg::print("[", boost::this_thread::get_id(), "] Error: ", ec, hlib::logg::endl);
            }
            break;
        }
        catch(std::exception &ex) {
            hlib::logg::print("[", boost::this_thread::get_id(), "] Exception: ", ex.what(), hlib::logg::endl);
        }
    }

    hlib::logg::print("[", boost::this_thread::get_id(), "] Thread Finish", hlib::logg::endl);
}

void game::exit() {

}

//void game::reset_elapsed_time() {
//
//}

void game::run() {
    timer->start();
    begin_run();
}

//void game::run_once() {
//
//}
//
//void game::suppress_draw() {
//
//}
//
//void game::tick() {
//
//}
//
//void game::begin_draw() {
//
//}

void game::begin_run() {

    pt_draw = boost::posix_time::microsec_clock::local_time();
    pt_update = boost::posix_time::microsec_clock::local_time();
    io_service->post(boost::bind(&game::update, this, boost::posix_time::microsec_clock::local_time() - pt_update));
    window->Redraw();
    pt_update = boost::posix_time::microsec_clock::local_time();
    window->Run(true);
}

void game::draw(boost::posix_time::time_duration diff) {
    (void)diff;
}

//void game::end_draw() {
//
//}
//
//void game::end_run() {
//
//}

void game::initialize() {
    window->Init("Title", 250, 250, 250, 225, 0);
    window->PrintGLVersion();
    window->SetExposeCallback(
            [this](XEvent e) {
                (void)e;
                this->draw(boost::posix_time::microsec_clock::local_time() - pt_draw);
                pt_draw = boost::posix_time::microsec_clock::local_time();
            }
    );

    for(int i = 0; i < THREADS; i++) {
        worker_threads.create_thread(boost::bind(&workerfunc, io_service));
    }

    //timer->set_duration(1000/60);// 60 fps
    timer->set_duration(1000);// 1 fps
    timer->set_callback(
            [this](void* userdata)->void {
                (void)userdata;
                io_service->post(
                        boost::bind(&game::update, this, boost::posix_time::microsec_clock::local_time() - pt_update));
                window->Redraw();
                window->Flush();
                pt_update = boost::posix_time::microsec_clock::local_time();
            }
    );
    timer->set_repeting(true);
    timer->set_userdata((void*)this);
    //t.start();
}

void game::load_content() {

}

void game::on_activated() {

}

void game::on_deactivated() {

}

void game::on_exiting() {

}

void game::unload_content() {

}

void game::update(boost::posix_time::time_duration diff) {
    (void)diff;
}
