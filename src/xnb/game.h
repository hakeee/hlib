//
// Created by hakeee on 7/3/15.
//

#ifndef XNB_GAME_H
#define XNB_GAME_H

#include <iostream>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/bind.hpp>
#include "../wng/window.h"
#include "../net/timer.h"
#include "../wng/window.h"
#include "../wng/shader.h"


namespace hlib {
    namespace xnb {
        class game {

            boost::posix_time::ptime pt_update, pt_draw;

        public:
            void exit();

            //void reset_elapsed_time();

            void run();

            //void run_one_frame();

            //void suppress_draw();

            //void tick();

        protected:

            boost::shared_ptr<boost::asio::io_service> io_service;
            boost::shared_ptr<boost::asio::io_service::work> work;
            boost::shared_ptr<boost::asio::io_service::strand> strand;
            boost::shared_ptr<hlib::net::timer> timer;
            boost::shared_ptr<hlib::wng::window> window;
            boost::thread_group worker_threads;

            game():
                    io_service(new boost::asio::io_service),
                    work(new boost::asio::io_service::work(*io_service)),
                    strand(new boost::asio::io_service::strand(*io_service)),
                    timer(new hlib::net::timer(io_service, strand)),
                    window(new hlib::wng::window(":0")) {
            }

            //void begin_draw();

            void begin_run();

            virtual void draw(boost::posix_time::time_duration diff);

            //void end_draw();

            //void end_run();

            virtual void initialize();

            virtual void load_content();

            virtual void on_activated();

            virtual void on_deactivated();

            virtual void on_exiting();

            //void show_missing_requirement_message();

            virtual void unload_content();

            virtual void update(boost::posix_time::time_duration diff);
        };
    }
}

#endif //XNB_GAME_H